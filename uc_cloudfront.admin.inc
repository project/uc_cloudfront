<?php


function uc_cloudfront_admin_form() {
  $form = array();
  
  $form['uc_cloudfront_url'] = array(
    '#title' => t('CloudFront URL'),
    '#type' => 'textfield',
    '#default_value' => variable_get('uc_cloudfront_url', ""),
    '#required' => TRUE,
    '#description' => t('You may use the Resource URL provided by Amazon or you may use the CNAME that was created for this Resource URL'),
  );
  $form['uc_cloudfront_expire'] = array(
    '#title' => t('Expire download'),
    '#type' => 'select',
    '#default_value' => variable_get('uc_cloudfront_expire', 1),
    '#options' => drupal_map_assoc(range(1, 7)),
    '#required' => TRUE,
    '#description' => t('The number of days that the download should remain valid.'),
  );
  $form['uc_cloudfront_key_paired'] = array(
    '#title' => t('Key Paired ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('uc_cloudfront_key_paired', ""),
    '#required' => TRUE,
    '#description' => t('KeyPairedID currently provided by CloudFront'),
  );
  $form['uc_cloudfront_public_key'] = array(
    '#title' => t('Public SSH Key'),
    '#type' => 'textarea',
    '#default_value' => variable_get('uc_cloudfront_public_key', ""),
    '#required' => TRUE,
    '#description' => t('SSH public key currently setup on S3'),
  );
  $form['uc_cloudfront_private_key'] = array(
    '#title' => t('Private SSH key'),
    '#type' => 'textarea',
    '#default_value' => variable_get('uc_cloudfront_private_key', ""),
    '#required' => TRUE,
    '#description' => t('SSH private key currently setup on S3'),
  );
  
  return system_settings_form($form);

}

function uc_cloudfront_generate_urls($form_state) {
  $files = uc_cloudfront_list_files();

  $form['uc_cloudfront_generate'] = array(
    '#title' => 'Generate Signed URLs',
    '#type' => 'fieldset',
  );
  
  // Create a list of product titles that include files
  foreach ($files as $file) {
    $select[$file['title']] = $file['title'];
  }
  
  $form['uc_cloudfront_generate']['select'] = array(
    '#title' => 'Select Product Source',
    '#type' => 'select',
    '#options' => drupal_map_assoc($select),
    '#default_value' => $form_state['storage']['values']['select'],
  );
  
  // Allow up to 7 days to be set for link expire
  for($i = 1; $i <= 7; $i++) {
    $expires[$i] = $i . ' day';
  }
  
  $default_expire = $form_state['storage']['values']['expire'];
  $form['uc_cloudfront_generate']['expire'] = array(
    '#title' => 'Download Expire',
    '#type' => 'select',
    '#description' => 'The number of days that the download link will remain valid',
    '#options' => $expires,
    '#default_value' => $default_expire ? $default_expire : variable_get('uc_cloudfront_expire', 1),
  );
  
  // Match the videos from selected product to their filenames
  if($form_state['storage']['values']) {
    foreach($files as $file) {
	    if($file['title'] == $form_state['storage']['values']['select']) {
	      // Display a Signed URL for each available file
	      $form['uc_cloudfront_generate'][$file['filename']] = array(
	        '#title' => $file['filename'],
	        '#type' => 'textfield',
	        '#default_value' => uc_cloudfront_sign_url($file['filename'], $form_state['storage']['values']['expire']),
	      );
	    }
    }
  }
  
  $form['submit'] = array(
    '#value' => 'Generate URLs',
    '#type' => 'submit',
  );

  return $form;
}

/**
 * Implementation of hook_FORMID_submit()
 */
function uc_cloudfront_generate_urls_submit($form, &$form_state) {
  $form_state['storage']['values'] = $form_state['values'];

  //tell Drupal we are redrawing the same form
  $form_state['rebuild'] = TRUE;
}